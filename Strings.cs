﻿namespace Mikrusbot
{
    public static class Strings
    {
        public const string ErrorInvalidPanel = "Wystąpił błąd. Upewnij się że jesteś zalogowany używając polecenia `auth info`.";
        public const string ErrorNotLoggedIn = "Zaloguj się używając polecenia `auth link`.";

        public const string StatusDownloading = "Pobieranie danych...";
        public const string StatusDone = "Gotowe!";
    }
}