﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using HtmlAgilityPack;
using Mikrusbot.MikrusPanelCrawler;
using Mikrusbot.Utilities;
using MySqlConnector;

namespace Mikrusbot.Modules
{
    [Group("mik")]
    public class MikrusCommandsModule : ModuleBase<SocketCommandContext>
    {
        private readonly MySqlConnection _dbConn;

        public MikrusCommandsModule(MySqlConnection dbConn)
        {
            _dbConn = dbConn;
        }
        
        [Command("amfetamina")]
        [Summary("Aktywuje Amfetaminę")]
        public async Task Amfetamina()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var parsedResult = await OperationsUtilities.ExecPostOperationAsync(_dbConn, Context.User.Id, "amfetamina", "amfetamina");
            var successNode = parsedResult?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/p[@class=\"success\"]")
                              ?? parsedResult?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/p[2]");
            await message.QuickEdit(successNode is null ? Strings.ErrorInvalidPanel : successNode.InnerText);
        }

        [Command("tuntap")]
        [Summary("Aktywuje tun/tap/fuse na serwerze")]
        public async Task EnableTunTap()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var result = await OperationsUtilities.ExecSecretOpAndParseSuccessAsync(_dbConn, Context.User.Id, "tuntap");
            await message.QuickEdit(result);
        }

        [Command("reboot")]
        [Summary("Restartuje serwer")]
        public async Task Reboot()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var result = await OperationsUtilities.ExecSecretOpAndParseSuccessAsync(_dbConn, Context.User.Id, "restart");
            await message.QuickEdit(result);
        }
        
        [Command("warsztat")]
        [Summary("Pobiera klucze do warsztatu mikrusa")]
        public async Task Warsztat()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var parsedResult = await OperationsUtilities.ExecPostOperationAsync(_dbConn, Context.User.Id, "warsztat", "warsztat");
            var successNode = parsedResult?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/p[@class=\"success\"]");
            await message.QuickEdit(successNode is null ? Strings.ErrorInvalidPanel : successNode.InnerText);
        }
        
        [Command("resetpass")]
        [Summary("Resetuje hasło roota")]
        public async Task ResetRootPassword()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var result = await OperationsUtilities.ExecSecretOperationAsync(_dbConn, Context.User.Id, "pass");
            var passwordNode = result?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/h1");
            await message.QuickEdit(passwordNode is not null
                ? $"Gotowe! Twoje nowe hasło roota to ||{passwordNode.InnerText}||"
                : Strings.ErrorInvalidPanel);
        }

        [Command("db")]
        [Summary("Prośba o dane do wybranej bazy")]
        public async Task RequestDb([Summary("mongodb | mysql | postgres")] string database)
        {
            if (database != "mongodb" && database != "postgres" && database != "mysql")
            {
                await ReplyAsync($"Identyfikator bazy `{database}` jest niepoprawny.");
                return;
            }
            var message = await ReplyAsync("Wysyłanie prośby o dane...");
            var result = await OperationsUtilities.ExecSecretOpAndParseSuccessAsync(_dbConn, Context.User.Id, database);
            await message.QuickEdit(result);
        }
        
                [Command("tcpdel")]
        [Summary("Usuwa port mikrusa")]
        public async Task RemovePort(string port)
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var cookie = await DatabaseUtil.GetCookieByUserId(_dbConn, Context.User.Id);
            if (cookie == null)
            {
                await message.QuickEdit(Strings.ErrorNotLoggedIn);
                return;
            }
            var response = await RetreiverUtilities.RetreiveMikrusAsync(cookie, "?a=ports");
            var parsedResponse = new HtmlDocument();
            parsedResponse.LoadHtml(await response.Content.ReadAsStringAsync());
            var tbody = parsedResponse.DocumentNode.SelectSingleNode("//*[@id=\"porty\"]/tbody");
            if (tbody == null)
            {
                await message.QuickEdit(Strings.ErrorInvalidPanel);
                return;
            }

            var deleteNode = tbody.ChildNodes.FirstOrDefault(node => node.ChildNodes[0].FirstChild.InnerText == port)
                ?.ChildNodes[2].FirstChild;
            if (deleteNode == null)
            {
                await message.QuickEdit("Ten port nie istnieje.");
                return;
            }
            if (deleteNode.Name == "small")
            {
                await message.QuickEdit("Wybrany port jest portem domyślnym. Nie możesz go usunąć.");
                return;
            }

            var deleteQuery = deleteNode.Attributes["href"].Value.Replace("/panel/", string.Empty);
            await RetreiverUtilities.RetreiveMikrusAsync(cookie, deleteQuery);
            await message.QuickEdit("Żądanie usunięcia portu zostało wysłane.");
        }
        
        [Command("tcpadd")]
        [Summary("Dodaje nowy port do mikrusa")]
        public async Task NewPort()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var cookie = await DatabaseUtil.GetCookieByUserId(_dbConn, Context.User.Id);
            if (cookie == null)
            {
                await message.QuickEdit(Strings.ErrorNotLoggedIn);
                return;
            }
            var response = await RetreiverUtilities.RetreiveMikrusAsync(cookie, "?a=ports");
            var parsedResponse = new HtmlDocument();
            parsedResponse.LoadHtml(await response.Content.ReadAsStringAsync());
            var form = parsedResponse.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]//form");
            if (form == null)
            {
                await message.QuickEdit(parsedResponse.GetElementbyId("mainbox") == null ? Strings.ErrorNotLoggedIn : "Limit portów osiągnięty.");
                return;
            }
            var secret = form.ChildNodes.First(node => node.Attributes["type"]?.Value == "hidden").Attributes["value"]?.Value;
            await RetreiverUtilities.RetreiveMikrusPostAsync(cookie,
                new FormUrlEncodedContent(new[]
                    {new KeyValuePair<string, string>("x", secret), new KeyValuePair<string, string>("go", "go")}), "?a=ports");
            await message.QuickEdit("Żądanie dodania portu zostało wysłane.");
        }
        
        [Command("tcplist")]
        [Summary("Wyświetla listę portów mikrusa")]
        public async Task GetPorts()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var logEntriesEmbed = new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithTitle("Lista portów")
                .WithAuthor(Context.User);
            var parsedResponse =
                await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, Context.User.Id, "?a=ports");
            var tbody = parsedResponse?.DocumentNode.SelectSingleNode("//*[@id=\"porty\"]/tbody");
            if (tbody == null)
            {
                await message.QuickEdit(Strings.ErrorInvalidPanel);
                return;
            }
            var portEntries = new List<List<string>> {new() {"Port", "Port zewnętrzny", "Typ"}};
            
            portEntries.AddRange(
                from logEntry in tbody.ChildNodes
                select logEntry.ChildNodes into tdElems
                let localPort = tdElems[0].FirstChild.InnerText
                let publicPort = tdElems[1].FirstChild.InnerText
                let portType = tdElems[2].FirstChild.Name == "small" ? "TCP/UDP" : "TCP"
                select new List<string> {localPort, publicPort, portType}
            );

            await message.ModifyAsync(p =>
            {
                p.Content = "Gotowe!";
                p.Embed = logEntriesEmbed.WithDescription(MiscUtils.RenderTable(portEntries)).Build();
            });
        }

        [Command("reinstall")]
        [Summary("Dokonuje reinstalacji mikrusa")]
        public async Task Reinstall()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var parsedResponse = await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, Context.User.Id, "?a=reinstall");
            var dropdown = parsedResponse?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/form/select");
            if (dropdown == null)
            {
                await message.QuickEdit(Strings.ErrorInvalidPanel);
                return;
            }

            var interactionDropdownBuilder = new SelectMenuBuilder().WithCustomId("reinstall").WithPlaceholder("Wybierz obraz");
            foreach (var dropdownOption in dropdown.ChildNodes.Where(node => node.Name == "option"))
            {
                var imageName = dropdownOption.Attributes.First(attribute => attribute.Name == "value").Value;
                interactionDropdownBuilder.AddOption(dropdownOption.InnerText, imageName, imageName);
            }

            await message.ModifyAsync(properties =>
            {
                properties.Content = "Gotowe.";
                properties.Components = new ComponentBuilder().WithSelectMenu(interactionDropdownBuilder).Build();
            });
        }
    }
}