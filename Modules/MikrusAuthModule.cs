﻿using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using HtmlAgilityPack;
using Mikrusbot.MikrusPanelCrawler;
using Mikrusbot.Utilities;
using MySqlConnector;

namespace Mikrusbot.Modules
{
    [Group("auth")]
    public class MikrusAuthModule : ModuleBase<SocketCommandContext>
    {
        private readonly MySqlConnection _dbConn;

        public MikrusAuthModule(MySqlConnection dbConn)
        {
            _dbConn = dbConn;
        }
        
        [Command("link")]
        [Summary("Łączy konto Discord z kontem mikr.us.")]
        public async Task LinkAccount([Summary("link logowania")] string loginLink)
        {
            var message = await ReplyAsync("Logowanie...");
            var loginUri = ValidationUtilities.ValidateMikrusLink(loginLink);
            if (loginUri == null)
            {
                await message.QuickEdit("Podany link jest nieprawidłowy.");
                return;
            }

            var response = await RetreiverUtilities.RetreiveMikrusSessionAsync(loginUri);
            if (response.SessionCookie == null)
            {
                await message.QuickEdit("Nie udało się zalogować: serwer nie zwrócił ID sesji.");
                return;
            }

            var parsedResponse = new HtmlDocument();
            parsedResponse.LoadHtml(response.ResponseBody);
            if (parsedResponse.GetElementbyId("mainbox") != null)
            {
                await DatabaseUtil.InsertUser(_dbConn, Context.User.Id, response.SessionCookie.Value);
                await message.QuickEdit(Strings.StatusDone);
            }
            else
            {
                await message.QuickEdit("Nie udało się zalogować. Upewnij się że link jest poprawny i spróbuj ponownie.");
            }
        }
        
        [Command("info")]
        [Summary("Wyświetla informacje o podłączonym koncie mikr.us")]
        public async Task Profile()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var userId = Context.User.Id;
            var parsedResponse = await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, userId);
            string server;
            if (parsedResponse == null)
            {
                server = "\uD83D\uDD34 Brak podłączonego konta mikr.us.";
            }
            else
            {
                var parsedServer = parsedResponse.DocumentNode.SelectSingleNode("//*[@id=\"text01\"]/strong")
                    ?.InnerText;
                server = parsedServer == null
                    ? "\uD83D\uDFE1 Token dostępu wygasł. Zaloguj się ponownie używając polecenia `auth link`."
                    : $"\uD83D\uDFE2 {parsedServer}";
            }

            var resultEmbed = new EmbedBuilder()
                .WithColor(Color.Green)
                .WithTitle("Profil użytkownika mikr.us")
                .WithAuthor(Context.User)
                .WithDescription($"**Discord ID:** `{userId}`\n**Konto mikr.us:** {server}")
                .Build();
            await message.ModifyAsync(p =>
            {
                p.Content = "Gotowe!";
                p.Embed = resultEmbed;
            });
        }

        [Command("unlink")]
        [Summary("Rozłącza konto mikr.us z konta Discord.")]
        public async Task UnlinkAccount()
        {
            var cookie = await DatabaseUtil.GetCookieByUserId(_dbConn, Context.User.Id);
            if (cookie == null)
            {
                await ReplyAsync("Nie jesteś zalogowany.");
                return;
            }

            await RetreiverUtilities.RetreiveMikrusAsync(cookie, "?a=logout");
            await DatabaseUtil.DeleteUser(_dbConn, Context.User.Id);
            await ReplyAsync("Konto zostało rozłączone.");
        }
        
        [Command("chsrv")]
        [Summary("Wybiera innego mikrusa")]
        public async Task GetServers()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var parsedResponse = await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, Context.User.Id);
            var server = parsedResponse?.DocumentNode.SelectSingleNode("//*[@id=\"text01\"]/strong")?.InnerText;
            if (server == null)
            {
                await message.QuickEdit(Strings.ErrorInvalidPanel);
                return;
            }
            
            var serversNode = parsedResponse.DocumentNode.SelectSingleNode("//*[@id=\"innesrv\"]");
            if (serversNode == null)
            {
                await message.QuickEdit("Nie posiadasz innych mikrusów.");
            }
            else
            {
                var interactionDropdownBuilder = new SelectMenuBuilder().WithCustomId("chsrv").WithPlaceholder("Wybierz mikrusa");
                foreach (var serverNode in serversNode.ChildNodes)
                {
                    var mikrus = serverNode.FirstChild.InnerText;
                    interactionDropdownBuilder.AddOption(mikrus, mikrus);
                }
                await message.ModifyAsync(properties =>
                {
                    properties.Content = "Gotowe.";
                    properties.Components = new ComponentBuilder().WithSelectMenu(interactionDropdownBuilder).Build();
                });
            }
        }
    }
}