﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Mikrusbot.MikrusPanelCrawler;
using Mikrusbot.Utilities;
using MySqlConnector;

namespace Mikrusbot.Modules
{
    [Group("diag")]
    public class MikrusDiagModule : ModuleBase<SocketCommandContext>
    {
        private readonly MySqlConnection _dbConn;
        private static readonly Regex LogPathRegex = new("[0-9]+", RegexOptions.IgnoreCase|RegexOptions.Compiled);

        public MikrusDiagModule(MySqlConnection dbConn)
        {
            _dbConn = dbConn;
        }

        [Command("logs")]
        [Summary("Wyświetla listę logów na serwerze mikr.us")]
        public async Task GetLogs()
        {
            var message = await ReplyAsync("Pobieranie logów...");
            var logEntriesEmbed = new EmbedBuilder()
                .WithColor(Color.Blue)
                .WithTitle("Lista logów")
                .WithAuthor(Context.User);
            var parsedResponse =
                await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, Context.User.Id, "historia.php");
            var tbody = parsedResponse?.DocumentNode.SelectSingleNode("/table/tbody");
            if (tbody == null)
            {
                await message.QuickEdit(Strings.ErrorInvalidPanel);
                return;
            }
            var logEntries = new List<List<string>> {new() {"Data", "Operacja", "ID"}};
            
            logEntries.AddRange(
                from logEntry in tbody.ChildNodes where logEntry.Name == "tr"
                select logEntry.ChildNodes.Where(node => node.Name == "td").ToArray()
                into tdElems
                let date = tdElems[0].FirstChild.InnerText
                let operationType = tdElems[1].FirstChild.InnerText
                let logPath = tdElems[2].LastChild.Attributes["href"]?.Value
                let logId = logPath == null ? "w toku" : LogPathRegex.Match(logPath).Value
                select new List<string> {date, operationType, logId}
            );

            await message.ModifyAsync(p =>
            {
                p.Content = "Gotowe!";
                p.Embed = logEntriesEmbed.WithDescription(MiscUtils.RenderTable(logEntries)).Build();
            });
        }

        [Command("getlog")]
        [Summary("Pobiera treść logu")]
        public async Task GetLog([Summary("id logu")] string logId)
        {
            var message = await ReplyAsync($"Pobieranie logu `{logId}`...");
            var parsedResponse =
                await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, Context.User.Id, $"?a=logs&id={logId}");

            var logContent = parsedResponse?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/pre")?.InnerText;
            if (logContent == null)
            {
                await message.QuickEdit("Nie udało się pobrać logu. Upewnij się że jego ID jest poprawne.");
            }
            else
            {
                await message.QuickEdit(Strings.StatusDone);
                foreach (var page in logContent.SplitBy(1990))
                {
                    await ReplyAsync($"```{page}```");
                }
            }
        }
        
        [Command("ticket")]
        [Summary("Pozwala otworzyć nowy support ticket")]
        public async Task SupportTicket()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var parsedResponse = await RetreiverUtilities.RetreiveAndParsePanelAsync(_dbConn, Context.User.Id);
            var ticketLink = parsedResponse?.DocumentNode.SelectSingleNode("//*[@id=\"ticketOpen\"]/a")?.Attributes["href"]?.Value;
            await message.QuickEdit(ticketLink != null
                ? "Gotowe. Kliknij poniższy link aby otworzyć ticket\n"+ticketLink
                : Strings.ErrorInvalidPanel);
        }

        [Command("debug")]
        [Summary("Pobiera informacje diagnostyczne")]
        public async Task DebugDiag()
        {
            var message = await ReplyAsync(Strings.StatusDownloading);
            var parsedResult =
                await OperationsUtilities.ExecPostOperationAsync(_dbConn, Context.User.Id, "diagnostyka", "pomoc", "auth");
            var successNode = parsedResult?.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/p[2]/a");
            await message.QuickEdit(successNode is not null
                ? "Gotowe! Sprawdź poniższy link za około 3 minuty.\n" + successNode.InnerText
                : Strings.ErrorInvalidPanel);
        }
    }
}