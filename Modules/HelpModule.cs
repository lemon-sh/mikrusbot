﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace Mikrusbot.Modules
{
    [Group("help")]
    public class HelpModule : ModuleBase<SocketCommandContext>
    {
        private static readonly Embed GuideEmbed = new EmbedBuilder()
            .WithTitle("Mikrusbot - Pomoc")
            .WithColor(Color.Gold)
            .WithDescription("Mikrusbot to Discordowy bot pozwalający na łatwe zarządzanie twoim serwerem mikr.us.")
            .WithFooter("gitlab.com/lemon-sh/mikrusbot", "https://cdn.discordapp.com/attachments/830525468331081758/835947429412339793/gitlab-icon-rgba.png")
            .WithFields(
                new EmbedFieldBuilder().WithName("Łączenie kont")
                    .WithValue(
                        "Aby zacząć korzystać z bota, musisz połączyć swoje konto Discord z kontem panelu mikr.us. Zrobisz to za pomocą polecenia `auth link`."),
                new EmbedFieldBuilder().WithName("Kontekst")
                    .WithValue(
                        "Każda komenda ograniczona jest do określonego kontekstu (Serwer lub DM). Użyj komendy `help cmds` aby uzyskać listę komend dostępnych w danym kontekście.")
            ).Build();

        private readonly CommandService _commandService;

        public HelpModule(CommandService service)
        {
            _commandService = service;
        }

        private Embed GenerateHelp()
        {
            var helpContentBuilder = new EmbedBuilder
            {
                Title = "Mikrusbot - Komendy", Color = Color.Green,
                Description = "Lista dostępnych komend:"
            };
            
            foreach (var module in _commandService.Modules)
            {
                var helpDescriptionBuilder = new StringBuilder();
                foreach (var command in module.Commands)
                {
                    helpDescriptionBuilder.Append($"\n\u2800\u2022 **{command.Name}**\n\u2800\u2800__Opis__: *{command.Summary}*");
                    if (!command.Parameters.Any()) continue;
                    helpDescriptionBuilder.Append("\n\u2800\u2800__Parametry__:\n\u2800\u2800`");
                    helpDescriptionBuilder.Append(command.Name);
                    foreach (var parameter in command.Parameters)
                    {
                        helpDescriptionBuilder.Append(" <");
                        helpDescriptionBuilder.Append(parameter.Summary ?? parameter.Name);
                        helpDescriptionBuilder.Append('>');
                    }

                    helpDescriptionBuilder.Append('`');
                }
                
                helpContentBuilder.AddField(
                    module.Group == null ? "Inne komendy" : $"\u27A1 {CommandHandler.Prefix}{module.Group}",
                    helpDescriptionBuilder.ToString());
            }

            return helpContentBuilder.Build();
        }

        [Command("cmds")]
        [Summary("Wyświetla listę komend.")]
        public Task Help()
        {
            return ReplyAsync(embed: GenerateHelp());
        }

        [Command("guide")]
        [Summary("Wyświetla pomoc bota.")]
        public Task Guide()
        {
            return ReplyAsync(embed: GuideEmbed);
        }
        
        [Command("bug")]
        [Summary("Zgłasza błąd z mikrusbotem (nie z mikrusem!)")]
        public async Task BugReport([Summary("opis błędu, może być wiele linii")] [Remainder] string description)
        {
            if (Program.BugReportChannel == null)
            {
                await ReplyAsync("Moduł raportowania błędów jest aktualnie wyłączony.");
                return;
            }

            var reportEmbed = new EmbedBuilder().WithColor(Color.Red)
                .WithAuthor(Context.User).WithTitle("Bug report").WithDescription(description).WithCurrentTimestamp()
                .Build();
            await Program.BugReportChannel.SendMessageAsync(embed: reportEmbed);
            await ReplyAsync("Wysłano zgłoszenie!", embed: reportEmbed);
        }
    }
}