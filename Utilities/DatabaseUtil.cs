﻿using System.Threading.Tasks;
using MySqlConnector;
using Serilog;

namespace Mikrusbot.Utilities
{
    public static class DatabaseUtil
    {
        public static async Task<string> GetCookieByUserId(MySqlConnection dbConn, ulong userId)
        {
            string cookie;
            await dbConn.OpenAsync();
            await using (var cmd = new MySqlCommand())
            {
                cmd.Connection = dbConn;
                cmd.CommandText = "select cookie from users where discord_id=@discordId";
                cmd.Parameters.AddWithValue("discordId", userId);
                cookie = (string) await cmd.ExecuteScalarAsync();
            }

            await dbConn.CloseAsync();
            Log.Verbose("Cookie retreived for user {UserId}", userId);
            return cookie;
        }
        
        public static async Task DeleteUser(MySqlConnection dbConn, ulong userId)
        {
            await dbConn.OpenAsync();
            await using (var cmd = new MySqlCommand())
            {
                cmd.Connection = dbConn;
                cmd.CommandText = "delete from users where discord_id=@discordId";
                cmd.Parameters.AddWithValue("discordId", userId);
                await cmd.ExecuteNonQueryAsync();
            }
            await dbConn.CloseAsync();
            Log.Verbose("User {UserId} removed from database", userId);
        }

        // UWAGA: jeśli wystąpi duplikat, ta metoda usuwa poprzedniego użytkownika i wstawia nowego
        public static async Task InsertUser(MySqlConnection dbConn, ulong userId, string cookie)
        {
            await dbConn.OpenAsync();
            await using (var cmd = new MySqlCommand())
            {
                cmd.Connection = dbConn;
                cmd.CommandText = "replace into users(discord_id, cookie) values (@userid, @cookie)";
                cmd.Parameters.AddWithValue("userid", userId);
                cmd.Parameters.AddWithValue("cookie", cookie);
                await cmd.ExecuteNonQueryAsync();
            }

            await dbConn.CloseAsync();
            Log.Verbose("User {UserId} inserted into database", userId);
        }
    }
}