﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Serilog.Events;

namespace Mikrusbot.Utilities
{
    public static class MiscUtils
    {
        public static string RenderTable(IReadOnlyList<IReadOnlyList<string>> table)
        {
            var columnCount = table[0].Count;
            var rowCount = table.Count;
            var columnSizes = new int[columnCount];
            for (var i = 0; i < columnCount; i++)
            {
                var maxLength = 0;
                for (var j = 0; j < rowCount; j++)
                {
                    var currentStringLength = table[j][i].Length;
                    if (currentStringLength > maxLength) maxLength = currentStringLength;
                }

                columnSizes[i] = maxLength;
            }

            var rowSeparatorBuilder = new StringBuilder("+");
            for (var i = 0; i < columnCount; i++)
            {
                var dashLength = columnSizes[i] + 2;
                rowSeparatorBuilder.Append(new string('-', dashLength)).Append('+');
            }

            var rowSeparator = rowSeparatorBuilder.Append('\n').ToString();
            var outputBuilder = new StringBuilder("```elm\n");
            outputBuilder.Append(rowSeparator);
            for (var i = 0; i < rowCount; i++)
            {
                outputBuilder.Append('|');
                for (var j = 0; j < columnCount; j++)
                {
                    var currentCell = table[i][j];
                    currentCell = currentCell.PadRight(columnSizes[j]);
                    outputBuilder.Append($" {currentCell} |");
                }

                outputBuilder.Append('\n');
                outputBuilder.Append(rowSeparator);
            }

            return outputBuilder.Append("```").ToString();
        }

        public static LogEventLevel TranslateDiscordLogLevel(LogSeverity discordLogLevel)
        {
            return discordLogLevel switch
            {
                LogSeverity.Critical => LogEventLevel.Fatal,
                LogSeverity.Error => LogEventLevel.Error,
                LogSeverity.Warning => LogEventLevel.Warning,
                LogSeverity.Info => LogEventLevel.Information,
                LogSeverity.Verbose => LogEventLevel.Verbose,
                LogSeverity.Debug => LogEventLevel.Debug,
                _ => throw new ArgumentException("Invalid discordLogLevel")
            };
        }
    }

    public static class StringPagination
    {    
        public static IEnumerable<string> SplitBy(this string str, int chunkLength)
        {
            if (string.IsNullOrEmpty(str)) throw new ArgumentException("str cannot be empty");
            if (chunkLength < 1) throw new ArgumentException("chunkLength must be greater than 0");

            for (var i = 0; i < str.Length; i += chunkLength)
            {
                if (chunkLength + i > str.Length)
                    chunkLength = str.Length - i;

                yield return str.Substring(i, chunkLength);
            }
        }
    }

    public static class MessageQuickEdit
    {
        public static Task QuickEdit(this IUserMessage message, string newContent)
        {
            return message.ModifyAsync(properties => { properties.Content = newContent; });
        }
    }
}