﻿using System;

namespace Mikrusbot.MikrusPanelCrawler
{
    public static class ValidationUtilities
    {
        // zwraca Uri jeśli walidacja się powiedzie, w przeciwnym wypadku null
        public static Uri ValidateMikrusLink(string mikrusLink)
        {
            Uri resultUri;
            try
            {
                resultUri = new Uri(mikrusLink);
            }
            catch (UriFormatException)
            {
                return null;
            }

            if (resultUri.Host != "mikr.us") return null;
            return resultUri.LocalPath != "/panel/" ? null : resultUri;
        }
    }
}