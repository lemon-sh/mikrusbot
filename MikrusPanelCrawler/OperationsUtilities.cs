﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Mikrusbot.Utilities;
using MySqlConnector;

namespace Mikrusbot.MikrusPanelCrawler
{
    public static class OperationsUtilities
    {
        // SecretOperation - operacja wymagająca sekretu (?x=[random])
        public static async Task<HtmlDocument> ExecSecretOperationAsync(MySqlConnection dbConn, ulong userId, string service)
        {
            var cookie = await DatabaseUtil.GetCookieByUserId(dbConn, userId);
            if (cookie == null)
                return null;
            var secret = await RetreiverUtilities.GetSecret(service, cookie);
            var result = await RetreiverUtilities.RetreiveMikrusAsync(cookie, $"?a={service}&{secret}");
            var parsedResult = new HtmlDocument();
            parsedResult.LoadHtml(await result.Content.ReadAsStringAsync());
            return parsedResult;
        }

        public static async Task<string> ExecSecretOpAndParseSuccessAsync(MySqlConnection dbConn, ulong userId,
            string service)
        {
            var parsedResult = await ExecSecretOperationAsync(dbConn, userId, service);
            if (parsedResult == null) return Strings.ErrorNotLoggedIn;
            var successNode = parsedResult.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/p[@class=\"success\"]");
            return successNode is not null
                ? successNode.InnerText
                : Strings.ErrorInvalidPanel;
        }

        public static async Task<HtmlDocument> ExecPostOperationAsync(MySqlConnection dbConn, ulong userId, string postEndpoint, string service, string hiddenName = "x")
        {
            var cookie = await DatabaseUtil.GetCookieByUserId(dbConn, userId);
            if (cookie == null)
            {
                return null;
            }

            var secret = await RetreiverUtilities.GetPostSecret(service, cookie);
            var result = await RetreiverUtilities.RetreiveMikrusPostAsync(cookie,
                new FormUrlEncodedContent(new[] {new KeyValuePair<string, string>(hiddenName, secret)}),
                $"?a={postEndpoint}");
            var parsedResult = new HtmlDocument();
            parsedResult.LoadHtml(await result.Content.ReadAsStringAsync());
            return parsedResult;
        }
    }
}