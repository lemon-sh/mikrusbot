﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Mikrusbot.Utilities;
using MySqlConnector;
using Serilog;

namespace Mikrusbot.MikrusPanelCrawler
{
    public static class RetreiverUtilities
    {
        private static readonly Uri MikrusCookieDomain = new("https://mikr.us");
        private const string MikrusPanelEndpoint = "https://mikr.us/panel/";

        /*
         * Pobiera https://mikr.us/panel z wprowadzonym ciastkiem
         * UWAGA: nie można zagwarantować że ciastko zadziała, dlatego powinno się sprawdzić
         * czy serwer nie wysłał przypadkiem strony logowania zamiast panelu (ValidateMikrusResponse).
         */
        public static Task<HttpResponseMessage> RetreiveMikrusAsync(string sessionCookie, string query = "")
        {
            var cookies = new CookieContainer();
            cookies.Add(MikrusCookieDomain, new Cookie("PHPSESSID", sessionCookie));

            var httpClient = new HttpClient(new HttpClientHandler
            {
                CookieContainer = cookies,
                AllowAutoRedirect = true
            });
            var fullPath = MikrusPanelEndpoint + query;
            Log.Verbose("GET request to {ReqPath}", fullPath);
            return httpClient.GetAsync(fullPath);
        }

        // Loguje się do panelu za pomocą podanego linku logowania
        public static async Task<SessionCookieResponse> RetreiveMikrusSessionAsync(Uri link)
        {
            var cookies = new CookieContainer();
            var httpClient = new HttpClient(new HttpClientHandler
            {
                CookieContainer = cookies,
                AllowAutoRedirect = true
            });
            Log.Verbose("GET request to {ReqPath}", link.ToString());
            var responseMessage = await httpClient.GetAsync(link);
            return new SessionCookieResponse
            {
                SessionCookie =
                    cookies.GetCookies(MikrusCookieDomain)
                        .FirstOrDefault(cookie => cookie.Name == "PHPSESSID"),
                ResponseBody = await responseMessage.Content.ReadAsStringAsync()
            };
        }

        public static Task<HttpResponseMessage> RetreiveMikrusPostAsync(string sessionCookie,
            HttpContent requestContent, string query = "")
        {
            var cookies = new CookieContainer();
            cookies.Add(MikrusCookieDomain, new Cookie("PHPSESSID", sessionCookie));

            var httpClient = new HttpClient(new HttpClientHandler
            {
                CookieContainer = cookies,
                AllowAutoRedirect = true
            });
            var fullPath = MikrusPanelEndpoint + query;
            Log.Verbose("POST request to {ReqPath}", fullPath);
            return httpClient.PostAsync(fullPath, requestContent);
        }

        public static async Task<string> GetSecret(string service, string sessionCookie)
        {
            var response = await RetreiveMikrusAsync(sessionCookie, $"?a={service}");
            var responseBody = await response.Content.ReadAsStringAsync();
            var m = Regex.Match(responseBody, "x=[0-9a-f]+", RegexOptions.IgnoreCase);
            return m.Value;
        }

        public static async Task<string> GetPostSecret(string service, string sessionCookie)
        {
            var response = await RetreiveMikrusAsync(sessionCookie, $"?a={service}");
            var responseBody = await response.Content.ReadAsStringAsync();
            var parsedResult = new HtmlDocument();
            parsedResult.LoadHtml(responseBody);
            var form = parsedResult.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]//form");
            return form.ChildNodes.First(node => node.Attributes["type"]?.Value == "hidden").Attributes["value"]?.Value;
        }
        
        public static async Task<HtmlDocument> RetreiveAndParsePanelAsync(MySqlConnection dbConn, ulong userId,
            string query = "")
        {
            var cookie = await DatabaseUtil.GetCookieByUserId(dbConn, userId);
            if (cookie == null) return null;

            var response = await RetreiveMikrusAsync(cookie, query);
            var parsedResponse = new HtmlDocument();
            parsedResponse.LoadHtml(await response.Content.ReadAsStringAsync());
            return parsedResponse;
        }

        public class SessionCookieResponse
        {
            public string ResponseBody;
            public Cookie SessionCookie;
        }
    }
}