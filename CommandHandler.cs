﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using HtmlAgilityPack;
using Microsoft.Extensions.DependencyInjection;
using Mikrusbot.MikrusPanelCrawler;
using Mikrusbot.Utilities;
using MySqlConnector;
using Serilog;

namespace Mikrusbot
{
    public class CommandHandler
    {
        internal static string Prefix;
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly IServiceProvider _serviceProvider;

        public CommandHandler(DiscordSocketClient client, CommandService commands)
        {
            _client = client;
            _commands = commands;
            _serviceProvider = new ServiceCollection()
                .AddTransient(_ => new MySqlConnection(Program.MysqlConnectionString))
                .BuildServiceProvider();
        }

        public async Task InstallCommandsAsync()
        {
            _commands.CommandExecuted += OnCommandExecutedAsync;
            _client.MessageReceived += HandleCommandAsync;
            _client.InteractionCreated += HandleInteractions;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _serviceProvider);
            Log.Information("{ModuleCount} modules registered ({CommandCount} commands)", _commands.Modules.Count(), _commands.Commands.Count());
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            if (messageParam is not SocketUserMessage message) return;
            
            var argPos = 0;
            if (!message.Author.IsBot && message.HasStringPrefix(Prefix, ref argPos))
            {
                if (message.Channel is not IPrivateChannel)
                {
                    await message.Channel.SendMessageAsync(
                        $"{message.Author.Mention} Z powodów bezpieczeństwa, tego bota można używać tylko w wiadomościach prywatnych (DM).");
                    await message.DeleteAsync();
                    return;
                }
                await _commands.ExecuteAsync(new SocketCommandContext(_client, message), argPos, _serviceProvider);
            }
        }

        private static async Task OnCommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, IResult result)
        {
            if (command.IsSpecified)
                Log.Verbose("Command {Cmd} finished (fired by user {Id}:{Name})", $"{command.Value.Module.Group} {command.Value.Name}", context.User.Id, context.User.ToString());
            if (!result.IsSuccess)
                switch (result.Error)
                {
                    case CommandError.UnknownCommand:
                        await context.Channel.SendMessageAsync(
                            $"Nieznane polecenie. Wpisz `{Prefix}help cmds` aby zobaczyć dostępne komendy.");
                        break;
                    case CommandError.BadArgCount:
                    case CommandError.ParseFailed:
                        await context.Channel.SendMessageAsync("Błąd składni.");
                        break;
                    case CommandError.Exception:
                        await context.Channel.SendMessageAsync("Wystąpił nieoczekiwany wyjątek.");
                        if (result is ExecuteResult executeResult)
                            Log.Error(executeResult.Exception, "An exception has occurred");
                        break;
                    default:
                        await context.Channel.SendMessageAsync(
                            $"Wystąpił nieoczekiwany błąd: ```{result.ErrorReason}```");
                        break;
                }
        }

        private async Task HandleInteractions(SocketInteraction rawInteraction)
        {
            if (rawInteraction is not SocketMessageComponent interaction) return;
            await using var dbConn = (MySqlConnection)_serviceProvider.GetService(typeof(MySqlConnection));
            switch (interaction.Data.CustomId)
            {
                case "reinstall":
                {
                    var image = interaction.Data.Values.First();
                    var message = await interaction.Channel.SendMessageAsync($"Uruchamianie instalacji `{image}`...");
                    await interaction.Message.DeleteAsync();
                    var cookie = await DatabaseUtil.GetCookieByUserId(dbConn, interaction.User.Id);
                    if (cookie == null)
                    {
                        await message.QuickEdit(Strings.ErrorNotLoggedIn);
                        return;
                    }

                    var secret = await RetreiverUtilities.GetPostSecret("reinstall", cookie);
                    var result = await RetreiverUtilities.RetreiveMikrusPostAsync(cookie,
                        new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("x", secret),
                            new KeyValuePair<string, string>("distro", image)
                        }), "?a=reinstall");
                    var parsedResult = new HtmlDocument();
                    parsedResult.LoadHtml(await result.Content.ReadAsStringAsync());
                    var successNode = parsedResult.DocumentNode.SelectSingleNode("//*[@id=\"mainbox\"]/p[@class=\"success\"]");
                    await message.QuickEdit(successNode is not null
                        ? successNode.InnerText
                        : Strings.ErrorInvalidPanel);
                    break;
                }
                case "chsrv":
                {
                    var id = interaction.Data.Values.First();
                    var message = await interaction.Channel.SendMessageAsync(Strings.StatusDownloading);
                    await interaction.Message.DeleteAsync();
                    var cookie = await DatabaseUtil.GetCookieByUserId(dbConn, interaction.User.Id);
                    if (cookie == null)
                    {
                        await message.QuickEdit(Strings.ErrorNotLoggedIn);
                        return;
                    }
                    var response = await RetreiverUtilities.RetreiveMikrusAsync(cookie);
                    var parsedResponse = new HtmlDocument();
                    parsedResponse.LoadHtml(await response.Content.ReadAsStringAsync());
                    var serversNode = parsedResponse.DocumentNode.SelectSingleNode("//*[@id=\"innesrv\"]");
                    if (serversNode == null)
                    {
                        await message.QuickEdit("Nie posiadasz innych serwerów.");
                        return;
                    }
                    var switchLink = serversNode.ChildNodes.FirstOrDefault(node => node.FirstChild.InnerText == id);
                    if (switchLink == null)
                    {
                        await message.QuickEdit($"Nie posiadasz serwera {id}.");
                        return;
                    }
                    var switchQuery = switchLink.FirstChild.Attributes["href"].Value.Replace("/panel/", string.Empty);
                    await RetreiverUtilities.RetreiveMikrusAsync(cookie, switchQuery);
                    await message.QuickEdit($"Serwer {id} został wybrany.");
                    break;
                }
            }
        }
    }
}