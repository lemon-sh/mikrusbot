﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Mikrusbot.Utilities;
using MySqlConnector;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using Tomlyn;
using Tomlyn.Model;

namespace Mikrusbot
{
    internal static class Program
    {
        public static string MysqlConnectionString;
        public static SocketTextChannel BugReportChannel;
        private static DiscordSocketClient _client;
        private static CommandHandler _commandHandler;

        private static readonly AnsiConsoleTheme LemonTheme = new(new Dictionary<ConsoleThemeStyle, string>
        {
            [ConsoleThemeStyle.Text] = "\u001B[38;5;0015m",
            [ConsoleThemeStyle.SecondaryText] = "\u001B[38;5;0007m",
            [ConsoleThemeStyle.TertiaryText] = "\u001B[38;5;0008m",
            [ConsoleThemeStyle.Invalid] = "\u001B[38;5;0011m",
            [ConsoleThemeStyle.Null] = "\u001B[38;5;0027m",
            [ConsoleThemeStyle.Name] = "\u001B[38;5;0007m",
            [ConsoleThemeStyle.String] = "\u001B[38;5;0112m",
            [ConsoleThemeStyle.Number] = "\u001B[38;5;0202m",
            [ConsoleThemeStyle.Boolean] = "\u001B[38;5;0214m",
            [ConsoleThemeStyle.Scalar] = "\u001B[38;5;0085m",
            [ConsoleThemeStyle.LevelVerbose] = "\u001B[38;5;0212m",
            [ConsoleThemeStyle.LevelDebug] = "\u001B[38;5;0212m",
            [ConsoleThemeStyle.LevelInformation] = "\u001B[38;5;0083m",
            [ConsoleThemeStyle.LevelWarning] = "\u001B[38;5;0011m",
            [ConsoleThemeStyle.LevelError] = "\u001B[38;5;0015m\u001B[48;5;0196m",
            [ConsoleThemeStyle.LevelFatal] = "\u001B[38;5;0015m\u001B[48;5;0196m"
        });

        private static async Task Main()
        {
            if (!File.Exists("config.toml"))
            {
                Console.WriteLine("Config file does not exist [config.toml].");
                return;
            }

            var logBuilder = new LoggerConfiguration().WriteTo.Console(theme: LemonTheme);
            string discordToken;
            string reportChannelId;
            try
            {
                var config = (TomlTable) Toml.Parse(await File.ReadAllTextAsync("config.toml")).ToModel()["bot"];
                discordToken          = (string) config["discord_token"];
                CommandHandler.Prefix = (string) config["bot_prefix"];
                MysqlConnectionString = (string) config["mysql_connection_string"];
                reportChannelId       = (string) config["report_channel"];
                if ((bool) config["verbose_log"])
                {
                    logBuilder.MinimumLevel.Verbose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error has occurred while reading the config file:\n{e}");
                return;
            }

            Log.Logger = logBuilder.CreateLogger();
            Log.Verbose("Verbose logging enabled");
            Log.Information("Bot prefix set to {Prefix}", CommandHandler.Prefix);
            _client = new DiscordSocketClient();
            _client.Log += message =>
            {
                Log.Write(MiscUtils.TranslateDiscordLogLevel(message.Severity), message.Exception,
                    "{MessageSource}: {Message}", message.Source, message.Message);
                return Task.CompletedTask;
            };
            _client.Ready += () =>
            {
                if (BugReportChannel != null) return Task.CompletedTask;
                BugReportChannel = ulong.TryParse(reportChannelId, out var reportChannelIdLong)
                    ? (SocketTextChannel) _client.GetChannel(reportChannelIdLong) : null;
                if (BugReportChannel == null)
                    Log.Warning("Bug reporting channel ID {ChannelId} is not valid. Users will not be able to report bugs", reportChannelId);
                return Task.CompletedTask;
            };
            
            await _client.LoginAsync(TokenType.Bot, discordToken);
            await _client.StartAsync();
            try
            {
                var conn = new MySqlConnection(MysqlConnectionString);
                await conn.OpenAsync();
                var command = new MySqlCommand
                {
                    Connection = conn,
                    CommandText = "CREATE TABLE IF NOT EXISTS `users` (`id` INT NOT NULL AUTO_INCREMENT, `discord_id` BIGINT UNSIGNED NOT NULL, `cookie` TINYTEXT NOT NULL, PRIMARY KEY (`id`), UNIQUE (`discord_id`))"
                };
                await command.ExecuteNonQueryAsync();
                Log.Information("Database ready. [{Server} server, {Database} database]", conn.ServerVersion, conn.Database);
                await conn.CloseAsync();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Couldn't connect to the database. Make sure the connection string is valid");
                return;
            }
            
            _commandHandler = new CommandHandler(_client, new CommandService());
            await _commandHandler.InstallCommandsAsync();
            await Task.Delay(-1);
        }
    }
}